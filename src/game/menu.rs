use crate::game::player::*;
use crate::GameState;
use bevy_time::FixedTimestep;
use bevy::ecs::schedule::ShouldRun;
use bevy::input::keyboard::KeyboardInput;
use bevy::input::ButtonState::Pressed;
use bevy::prelude::*;
pub struct OptionPlugin;

impl Plugin for OptionPlugin {
    fn build(&self, app: &mut App) {
        app
            //.add_startup_system(setup.system())
            //.add_system_set(
            //      SystemSet::on_enter(GameState::Playing)
            //        //.with_system(spawn_ui.system().label("spawn_ui"))
            //      .with_system(spawn_players.system().label("spawn").after("spawn_ui"))
            //    .with_system(spawn_camera.system().after("spawn")),
            //)
            .add_system_set(SystemSet::on_update(GameState::Playing).with_system(event_change))
            .add_system_set(SystemSet::new().with_system(change).with_run_criteria(
                FixedTimestep::step(0.5).chain(
                    |In(input): In<ShouldRun>, state: Res<State<GameState>>| {
                        if state.current() == &GameState::Playing {
                            input
                        } else {
                            ShouldRun::No
                        }
                    },
                ),
            ))
            .add_system_set(SystemSet::on_update(GameState::Playing2).with_system(event_change))
            .add_system_set(SystemSet::new().with_system(change).with_run_criteria(
                FixedTimestep::step(0.5).chain(
                    |In(input): In<ShouldRun>, state: Res<State<GameState>>| {
                        if state.current() == &GameState::Playing2 {
                            input
                        } else {
                            ShouldRun::No
                        }
                    },
                ),
            ));
    }
}
fn change(kb: Res<Input<KeyCode>>) {
    if kb.pressed(KeyCode::Up) {
        //println!("Up");
    }
    if kb.pressed(KeyCode::Down) {
        //println!("Down");
    }
    if kb.pressed(KeyCode::Left) {
        //println!("Left");
    }
    if kb.pressed(KeyCode::Right) {
        //println!("Right");
    }
}
fn event_change(
    mut kbev: EventReader<KeyboardInput>,
    mut q: Query<&mut TankState, With<Tank>>,
    on_move: Res<OnMove>,
) {
    for i in kbev.iter() {
        if let (Some(keycode), state) = (i.key_code, i.state) {
            //println!("{:?}",keycode);
            //println!("{:?}",state);
            match keycode {
                KeyCode::Up => {
                    if state == Pressed {
                        for mut state in q.iter_mut() {
                            if state.index == on_move.0 {
                                state.power += 2;
                            }
                        }
                    } else {
                        //timer.up.stop()
                    }
                }
                KeyCode::Down => {
                    if state == Pressed {
                        for mut state in q.iter_mut() {
                            if state.index == on_move.0 {
                                state.power -= 2;
                            }
                        }
                    } else {
                        //timer.up.stop()
                    }
                }
                KeyCode::Right => {
                    if state == Pressed {
                        for mut state in q.iter_mut() {
                            if state.index == on_move.0 {
                                state.angel -= 2.;
                            }
                        }
                    } else {
                        //timer.up.stop()
                    }
                }
                KeyCode::Left => {
                    if state == Pressed {
                        for mut state in q.iter_mut() {
                            if state.index == on_move.0 {
                                state.angel += 2.;
                            }
                        }
                    } else {
                        //timer.up.stop()
                    }
                }
                _ => {}
            }
        }
    }
}
