use crate::glue::loading::TextureAssets;
use crate::GameState;
use bevy::prelude::*;
use core::f64::consts::PI;
use rand::prelude::SliceRandom;
use rand::Rng;

const TIME_STEP: f32 = 1. / 60.;
pub struct Materials {
    pub black: Color,
    pub uiblack: UiColor,
    pub trans: UiColor,
    bomb: Color,
    pub pause_button: ButtonMaterials,
}
pub struct HelpVis(pub bool);
pub struct WinSize {
    pub w: f32,
    pub h: f32,
}
pub struct OnMove(pub u32);

pub struct GameSettings {
    pub mode: usize,
    pub number: usize,
}

// region:Components
#[derive(Clone, Component)]
pub struct Wind(pub f64);
#[derive(Clone, Component)]
pub struct BombBase {
    color: Color,
    tankstate: TankState,
    translans: (f32, f32),
}
#[derive(Clone, Component)]
pub struct FireState {
    pub bombs: Vec<BombBase>,
}
#[derive(Clone, Component)]
pub struct TankState {
    pub name: String,
    pub index: u32,
    pub color: Color,
    pub angel: f64,
    pub life: i32,
    pub shield: i32,
    pub power: i32,
    pub weapons: Vec<String>,
    pub weapon_selected: i32,
}
#[derive(Component)]
struct Turr;
#[derive(Component)]
struct Bomb;
#[derive(Component)]
pub struct Tank(u32);
#[derive(Clone, Component)]
struct Startx {
    x: f64,
    y: f64,
    t: Time,
}
#[derive(Component)]
pub struct Turret {
    pub i: u32,
}
#[derive(Component)]
struct Speed(f32);
impl Default for Speed {
    fn default() -> Self {
        Self(50.0)
    }
}

pub struct ButtonMaterials {
    pub normal: Color,
    pub hovered: Color,
}

pub struct PlayerPlugin;

impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_startup_system(setup)
            .add_system_set(
                SystemSet::on_enter(GameState::Playing)
                    //.with_system(spawn_ui.label("spawn_ui"))
                    .with_system(spawn_players.label("spawn").after("spawn_ui"))
                    .with_system(spawn_camera.after("spawn")),
            )
            .add_system_set(
                SystemSet::on_update(GameState::Playing)
                    .with_system(bomb_movement)
                    .with_system(timed_player_movement)
                    .with_system(tank_fire),
            )
            //;
            .add_system_set(
                SystemSet::on_enter(GameState::Playing2)
                    //.with_system(spawn_ui.label("spawn_ui"))
                    .with_system(spawn_players.label("spawn").after("spawn_ui"))
                    .with_system(spawn_camera.after("spawn")),
            )
            .add_system_set(
                SystemSet::on_update(GameState::Playing2)
                    .with_system(bomb_movement)
                    .with_system(timed_player_movement)
                    .with_system(tank_fire4),
            );
    }
}

type BombQuery<'a> = (
    Entity,
    &'a TankState,
    &'a Speed,
    &'a mut Transform,
    &'a mut Startx,
    With<Bomb>,
);
fn bomb_movement(
    mut commands: Commands,
    win_size: Res<WinSize>,
    windres: Res<Wind>,
    mut bomb_query: Query<BombQuery>,
) {
    // for each laser from enemy
    for (entity, state, _speed, mut tf, mut start, _) in bomb_query.iter_mut() {
        let v_0: f64 = state.power as f64;

        let angel: f64 = state.angel;
        let wind = windres.0;
        let g: f64 = 15.;
        let q: f64 = (PI / 180.) * angel;
        start.t.update();

        let t = 2. * &start.t.seconds_since_startup();

        let x = v_0 * q.cos() * t + start.x + t * wind;
        let y = start.y + (v_0 * q.sin()) * t - 0.5 * g * (t * t);
        //println!("x:{}y:{}",x,y);
        tf.translation.x = x as f32;
        tf.translation.y = y as f32;

        if tf.translation.x > 500. || tf.translation.x < -500. {
            start.x -= 2. * tf.translation.x as f64;
        }

        if tf.translation.y < -win_size.h / 2. - 50. {
            commands.entity(entity).despawn();
        }
    }
}

fn tank_fire(
    mut commands: Commands,

    materials: Res<Materials>,
    mut resmut: (ResMut<FireState>, ResMut<Wind>, ResMut<OnMove>),
    settings: Res<GameSettings>,
    kb: Res<Input<KeyCode>>,
    mut query: Query<(&Tank, &Transform, &TankState, With<Tank>)>,
) {
    let mut vec = (0.0, 0.0);
    let mut bomb: TankState = TankState {
        name: "Error".to_string(),
        index: 1,
        angel: 00.0,
        color: Color::NONE,
        life: -5,
        shield: 1,
        power: 950,
        weapons: vec!["missile".to_string()],
        weapon_selected: 0,
    };
    for (player, transform, state, _) in query.iter_mut() {
        if player.0 == resmut.2 .0 {
            vec = (transform.translation.x, transform.translation.y);
            bomb = state.clone();
        }
    }
    if kb.just_pressed(KeyCode::Return) {
        resmut.0.bombs.push(BombBase {
            color: materials.bomb,
            tankstate: bomb,
            translans: (vec.0, vec.1),
        });

        if (resmut.2 .0 as usize) < (settings.number - 1) {
            resmut.2 .0 += 1;
        } else {
            resmut.2 .0 = 0;
        }
    }
    if resmut.0.bombs.len() == settings.number {
        for i in &resmut.0.bombs {
            commands
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        color: i.color,
                        custom_size: Some(Vec2::new(3., 3.)),
                        ..Default::default()
                    },
                    transform: Transform {
                        translation: Vec3::new(i.translans.0, i.translans.1 + 4., 1.),
                        ..Default::default()
                    },
                    ..Default::default()
                })
                .insert(Speed::default())
                .insert(Bomb)
                .insert(i.tankstate.clone())
                .insert(Startx {
                    x: i.translans.0 as f64,
                    y: (i.translans.1 + 4.) as f64,
                    t: Time::default(),
                });

            let mut rng = rand::thread_rng();
            resmut.1 .0 = rng.gen_range(-20..20) as f64;
        }
        resmut.0.bombs.clear();
    }
}

fn tank_fire4(
    mut commands: Commands,
    materials: Res<Materials>,
    mut on_move: ResMut<OnMove>,
    settings: Res<GameSettings>,
    kb: Res<Input<KeyCode>>,
    mut query: Query<(&Tank, &Transform, &TankState, With<Tank>)>,
) {
    let mut vec = (0.0, 0.0);
    let mut bomb: TankState = TankState {
        name: "Error".to_string(),
        index: 1,
        angel: 00.0,
        color: Color::NONE,
        life: -5,
        shield: 1,
        power: 950,
        weapons: vec!["missile".to_string()],
        weapon_selected: 0,
    };
    for (player, transform, state, _) in query.iter_mut() {
        if player.0 == on_move.0 {
            vec = (transform.translation.x, transform.translation.y);
            bomb = state.clone();
        }
    }
    if kb.just_pressed(KeyCode::Return) {
        commands
            .spawn_bundle(SpriteBundle {
                sprite: Sprite {
                    color: materials.bomb,
                    custom_size: Some(Vec2::new(3., 3.)),
                    ..Default::default()
                },
                transform: Transform {
                    translation: Vec3::new(vec.0, vec.1 + 4., 1.),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Speed::default())
            .insert(Bomb)
            .insert(bomb)
            .insert(Startx {
                x: vec.0 as f64,
                y: (vec.1 + 4.) as f64,
                t: Time::default(),
            });
        if (on_move.0 as usize) < (settings.number - 1) {
            on_move.0 += 1;
        } else {
            on_move.0 = 0;
        }
    }
}

fn timed_player_movement(
    keyboard: Res<Input<KeyCode>>,
    on_move: ResMut<OnMove>,
    mut query: Query<(&Tank, &Speed, &mut Transform, With<Tank>)>,
    mut turret: Query<(&Turret, &mut Transform), Without<Tank>>,
) {
    for (player, speed, mut transform, _) in query.iter_mut() {
        if player.0 == on_move.0 {
            let dir = if keyboard.pressed(KeyCode::H) {
                -1.
            } else if keyboard.pressed(KeyCode::L) {
                1.
            } else {
                0.
            };
            transform.translation.x += dir * speed.0 * TIME_STEP;
            for (i, mut t) in turret.iter_mut() {
                if i.i == on_move.0 {
                    t.translation.x += dir * speed.0 * TIME_STEP;
                }
            }
        }
    }
}

fn spawn_camera(mut commands: Commands) {
//    commands.spawn_bundle(Camera2dBundle::default());
}

fn spawn_players(
    mut commands: Commands,
    win_size: Res<WinSize>,
    tank: Res<TextureAssets>,
    settings: Res<GameSettings>,
) {
    let mut rng = rand::thread_rng();

    //println!("{}x{}",win_size.w,win_size.h);

    let tankh = 30.0;
    let tankv = 30.0;
    let widgh = win_size.w - tankh;
    let bottom = -(win_size.h - tankv) / 2.;

    //    while (position1 - position2) > -padding && (position1 - position2) < padding {
    //        position1 = rng.gen_range(0..widgh as i32) as f32;
    //        position2 = rng.gen_range(0..widgh as i32) as f32;
    //    }

    let names = vec![
        "Tank1", "Tank2", "Tank3", "Tank4", "Tank5", "Tank6", "Tank7", "Tank8", "Tank9", "Tank0",
    ];
    let colors = vec![
        Color::PURPLE,
        Color::GREEN,
        Color::WHITE,
        Color::YELLOW,
        Color::BLUE,
        Color::ORANGE,
        Color::GOLD,
        Color::VIOLET,
        Color::PINK,
        Color::RED,
    ];
    let powers = vec![100, 90, 70, 50, 30, 10, 30, 50, 70, 90];
    let angels = vec![100., 90., 70., 50., 30., 10., 30., 50., 70., 90.];

    let mut pos: Vec<f32> = vec![];
    let mut rangeend = (widgh / settings.number as f32) - (widgh / 2.);
    let mut rangestart = -(widgh / 2.);
    for _ in 0..settings.number {
        pos.push(rng.gen_range(rangestart as i32..rangeend as i32) as f32);
        rangeend += widgh / settings.number as f32;
        rangestart += widgh / settings.number as f32;
    }
    pos.shuffle(&mut rng);
    for i in 0..settings.number {
        commands
            .spawn_bundle(SpriteBundle {
                texture: tank.tank.clone(),
                sprite: Sprite {
                    custom_size: Some(Vec2::new(30., 30.)),
                    color: colors[i],
                    ..Default::default()
                },

                transform: Transform {
                    translation: Vec3::new(pos[i], bottom + 10.0, 10.),
                    //translation : Vec3::new(-100.,bottom+10.0,10.),
                    ..Default::default()
                },
                ..Default::default()
            })
            .insert(Tank(i as u32))
            .insert(TankState {
                name: names[i].to_string(),
                index: i as u32,
                color: colors[i],
                angel: angels[i],
                life: 100,
                shield: 0,
                power: powers[i],
                weapons: vec!["missile".to_string()],
                weapon_selected: 0,
            })
            .insert(Speed::default());

        commands
            .spawn_bundle(SpriteBundle {
                texture: tank.turret.clone(),
                sprite: Sprite {
                    color: colors[i],
                    custom_size: Some(Vec2::new(22., 22.)),
                    ..Default::default()
                },

                transform: Transform {
                    translation: Vec3::new(pos[i], bottom + 14.0, 11.),
                    rotation: Quat::from_rotation_z(
                        (core::f32::consts::PI / 180.) * (angels[i] as f32 - 90.),
                    ),
                    ..Default::default()
                },

                ..Default::default()
            })
            .insert(Turret { i: i as u32 });
    }
}
// hhkjhkjhjkhjjhk
fn setup(
    mut windows: ResMut<Windows>,
    _materials: ResMut<Assets<ColorMaterial>>,
    mut commands: Commands,
) {
    let window = windows.get_primary_mut().unwrap();
    commands.insert_resource(WinSize {
        w: window.width(),
        h: window.height(),
    });
    commands.insert_resource(Materials {
        bomb: Color::rgb(0.04, 0.04, 0.04),
        trans: UiColor(Color::rgba(0., 0., 0., 0.)),
        uiblack: UiColor(Color::rgba(0., 0., 0., 1.)),

        black: Color::rgba(0., 0., 0., 1.),
        pause_button: ButtonMaterials {
            normal: Color::rgb(0.15, 0.15, 0.15),
            hovered: Color::rgb(0.25, 0.25, 0.25),
        },
    });
    commands.insert_resource(OnMove(0));
    commands.insert_resource(Wind(0.));
    commands.insert_resource(HelpVis(false));
    commands.insert_resource(FireState { bombs: vec![] });
    commands.insert_resource(GameSettings { mode: 0, number: 2 });
}
