use crate::game::player::GameSettings;
use crate::game::player::Materials;
use crate::glue::loading::FontAssets;
use crate::GameState;
use bevy_ui::UiRect;
use bevy::prelude::*;
pub struct MenuPlugin;

impl Plugin for MenuPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<ButtonMaterials>()
            .add_system_set(SystemSet::on_enter(GameState::Menu).with_system(setup_menu))
            .add_system_set(
                SystemSet::on_update(GameState::Menu).with_system(click_play_button), //                    .with_system(enter_play.system()),
            );
    }
}

struct ButtonMaterials {
    normal: UiColor,
    _hovered: UiColor,
}

impl FromWorld for ButtonMaterials {
    fn from_world(_world: &mut World) -> Self {
        ButtonMaterials {
            normal: UiColor(Color::rgb(0.15, 0.15, 0.15)),
            _hovered: UiColor(Color::rgb(0.25, 0.25, 0.25)),
        }
    }
}
#[derive(Component)]
struct PlayButton;
#[derive(Component)]
struct Mode(usize);

fn setup_menu(
    materials: Res<Materials>,
    mut commands: Commands,
    font_assets: Res<FontAssets>,
    button_materials: Res<ButtonMaterials>,
) {
    commands.spawn_bundle(Camera2dBundle::default());
    //commands.spawn_bundle(UiCameraBundle::default());
    let button2 = commands
        .spawn_bundle(ButtonBundle {
            style: Style {
                size: Size::new(Val::Px(200.0), Val::Px(50.0)),
                margin: UiRect::all(Val::Auto),
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,
                ..Default::default()
            },
            color: button_materials.normal,
            ..Default::default()
        })
        .insert(PlayButton)
        .insert(Mode(1))
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text {
                    sections: vec![TextSection {
                        value: "4S, Normal".to_string(),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 30.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    }],
                    alignment: Default::default(),
                },
                ..Default::default()
            });
        })
        .id();

    let ui = commands
        .spawn_bundle(ButtonBundle {
            style: Style {
                size: Size::new(Val::Px(200.0), Val::Px(50.0)),
                margin: UiRect::all(Val::Auto),
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,
                ..Default::default()
            },
            color: button_materials.normal,
            ..Default::default()
        })
        .insert(PlayButton)
        .insert(Mode(0))
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text {
                    sections: vec![TextSection {
                        value: "2S, Alt".to_string(),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 30.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    }],
                    alignment: Default::default(),
                },
                ..Default::default()
            });
        })
        .id();

    commands
        .spawn_bundle(NodeBundle {
            color: materials.trans,
            style: Style {
                size: Size::new(Val::Percent(100.), Val::Percent(100.)),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Ui)
        .push_children(&[ui])
        .push_children(&[button2]);
}

type ButtonInteraction<'a> = (
    Entity,
    &'a Interaction,
    &'a mut Mode,
    //    &'a mut Handle<ColorMaterial>,
    &'a Children,
);
#[derive(Component)]
struct Ui;

fn enter_play(
    mut commands: Commands,
    mut state: ResMut<State<GameState>>,
    main_query: Query<Entity, With<Ui>>,
    mut kb: ResMut<Input<KeyCode>>,
) {
    if kb.just_released(KeyCode::Return) {
        //if
        let main = main_query.single();
        {
            commands.entity(main).despawn_recursive();
        }
        println!("woaw");
        state.set(GameState::WeaponSelect).unwrap();
        kb.reset(KeyCode::Return)
    }
}

fn click_play_button(
    mut commands: Commands,
    _button_materials: Res<ButtonMaterials>,
    mut state: ResMut<State<GameState>>,
    mut interaction_query: Query<ButtonInteraction, (Changed<Interaction>, With<Button>)>,
    main_query: Query<Entity, With<Ui>>,
    mut settings: ResMut<GameSettings>,
) {
    for (_button, interaction, mode, _children) in interaction_query.iter_mut() {
        match *interaction {
            Interaction::Clicked => {
                println!("woaw");
                let main = main_query.single();
                {
                    commands.entity(main).despawn_recursive();
                }
                state.set(GameState::WeaponSelect).unwrap();
                settings.mode = mode.0;
                if mode.0 == 0 {
                    settings.number = 2;
                } else {
                    settings.number = 4;
                }
            }
            Interaction::Hovered => {
                // *material = button_materials.hovered.clone();
            }
            Interaction::None => {
                // *material = button_materials.normal.clone();
            }
        }
    }
}
