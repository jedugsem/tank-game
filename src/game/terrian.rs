use crate::GameState;
use bevy::prelude::*;
use bevy_ecs_tilemap::prelude::*;
use crate::game::helpers;
use bevy_prototype_lyon::prelude::*;
pub struct TerrianPlugin;
impl Plugin for TerrianPlugin {
    fn build(&self, app: &mut App) {
        app
            //.add_plugin(TilemapPlugin)
            .add_system_set(
                SystemSet::on_enter(GameState::Playing)
                    .with_system(spawn_terrian.label("spawn").after("spawn_ui"))
//                    .with_system(spawn_players.system().label("spawn").after("spawn_ui"))
//                    .with_system(spawn_camera.system().after("spawn")),
            )
            .add_system_set(
                SystemSet::on_update(GameState::Playing)
             .with_system(helpers::camera::movement)
                .with_system(helpers::texture::set_texture_filters_to_nearest)
//                    .with_system(bomb_movement.system())
//                    .with_system(timed_player_movement.system())
//                    .with_system(tank_fire.system())
            )
            //;
            .add_system_set(
                SystemSet::on_enter(GameState::Playing2)
                    .with_system(spawn_terrian.label("spawn").after("spawn_ui"))
                    //.with_system(spawn_ui.system().label("spawn_ui"))
//                    .with_system(spawn_players.system().label("spawn").after("spawn_ui"))
//                    .with_system(spawn_camera.system().after("spawn")),
            )
            .add_system_set(
                SystemSet::on_update(GameState::Playing2)             .with_system(helpers::camera::movement)
                .with_system(helpers::texture::set_texture_filters_to_nearest)

//                    .with_system(bomb_movement.system())
//                    .with_system(timed_player_movement.system())
//                    .with_system(tank_fire4.system())
            );
    }
}

fn spawn_terrian(asset_server: Res<AssetServer>, mut commands: Commands, mut map_query: MapQuery) {
    println!("weaow");
    let texture_handle = asset_server.load("textures/tiles.png");

    // Create map entity and component:
    let map_entity = commands.spawn().id();
    let mut map = Map::new(0u16, map_entity);

    // Creates a new layer builder with a layer entity.
    let (mut layer_builder, _) = LayerBuilder::new(
        &mut commands,
        LayerSettings::new(
            MapSize(20, 20),
            ChunkSize(8, 8),
            TileSize(16.0, 16.0),
            TextureSize(96.0, 16.0),
        ),
        0u16,
        0u16,
    );

    layer_builder.set_all(TileBundle::default());

    // Builds the layer.
    // Note: Once this is called you can no longer edit the layer until a hard sync in bevy.
    let layer_entity = map_query.build_layer(&mut commands, layer_builder, texture_handle);

    // Required to keep track of layers for a map internally.
    map.add_layer(&mut commands, 0u16, layer_entity);

    // Spawn Map
    // Required in order to use map_query to retrieve layers/tiles.
    commands
        .entity(map_entity)
        .insert(map)
        .insert(Transform::from_xyz(-200., -200., 50.0))
        .insert(GlobalTransform::default());
}
