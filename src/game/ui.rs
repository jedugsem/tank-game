use crate::game::player::*;
use crate::glue::loading::FontAssets;
use crate::GameState;
use bevy::prelude::*;
use bevy_ui::UiRect;
use std::f32::consts::PI;

pub struct UiPlugin;

impl Plugin for UiPlugin {
    fn build(&self, app: &mut App) {
        app.add_system_set(
            SystemSet::on_enter(GameState::Playing)
                .with_system(spawn_bar.label("spawn_ui"))
                .with_system(spawn_help.label("spawn_ui")),
        )
        .add_system_set(
            SystemSet::on_update(GameState::Playing)
                .with_system(change_display)
                .with_system(display_help), //.with_system(pause.system())
        )
        .add_system_set(
            SystemSet::on_enter(GameState::Playing2)
                .with_system(spawn_bar.label("spawn_ui"))
                .with_system(spawn_help.label("spawn_ui")),
        )
        .add_system_set(
            SystemSet::on_update(GameState::Playing2)
                .with_system(change_display)
                .with_system(display_help), //.with_system(pause.system())
        );
    }
}
#[derive(Component)]
pub struct Display;
#[derive(Component)]
pub struct PauseButton;
#[derive(Component)]
struct PlayerDisplay;
#[derive(Component)]
struct LifeDisplay;
#[derive(Component)]
struct WindDisplay;
#[derive(Component)]
struct ShieldDisplay;
#[derive(Component)]
struct WeaponDisplay;
#[derive(Component)]
struct AngelDisplay;
#[derive(Component)]
struct SpeedDisplay;

#[derive(Component)]
struct HelpDisplay;

#[derive(Component)]
enum DisplayType {
    Player,
    Life,
    Shield,
    Weapon,
    Angel,
    Speed,
    Wind,
}
fn display_help(
    mut help_vis: ResMut<HelpVis>,
    help: Query<&Interaction, (Changed<Interaction>, With<PauseButton>)>,
    mut vis: Query<&mut Visibility, With<HelpDisplay>>,
) {
    if let Ok(help) = help.get_single() {
        match help {
            Interaction::Clicked => {
                if !help_vis.0 {
                    for mut i in vis.iter_mut() {
                        i.is_visible = true;
                        help_vis.0 = true;
                    }
                } else {
                    for mut i in vis.iter_mut() {
                        i.is_visible = false;
                        help_vis.0 = false;
                    }
                }
            }
            Interaction::Hovered => {}
            _ => {}
        }
    }
}
fn change_display(
    mut player: Query<(&mut Text, &DisplayType), With<Display>>,

    mut s: Query<(&mut TankState, With<Tank>)>,
    font_assets: Res<FontAssets>,
    on_move: Res<OnMove>,
    wind: Res<Wind>,
    mut q: Query<(&mut Transform, &Turret), With<Turret>>,
) {
    for (tank, _) in s.iter_mut() {
        if tank.index == on_move.0 {
            for (mut c, typ) in player.iter_mut() {
                c.sections = vec![match typ {
                    DisplayType::Wind => TextSection {
                        value: format!("{:>5}", wind.0),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 15.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    },

                    DisplayType::Player => TextSection {
                        value: format!("{:>5}", tank.name.clone()),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 15.0,
                            color: tank.color,
                        },
                    },
                    DisplayType::Life => TextSection {
                        value: format!("{}%", tank.life.to_string()),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 15.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    },
                    DisplayType::Weapon => TextSection {
                        value: format!(
                            "{}",
                            tank.weapons[tank.weapon_selected as usize].to_string()
                        ),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 15.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    },
                    DisplayType::Shield => TextSection {
                        value: format!("{}", tank.shield.clone()),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 15.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    },
                    DisplayType::Speed => TextSection {
                        value: format!("{}px/s", tank.power.clone()),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 15.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    },
                    DisplayType::Angel => TextSection {
                        value: {
                            for (mut turret, i) in q.iter_mut() {
                                if i.i == on_move.0 as u32 {
                                    turret.rotation = Quat::from_rotation_z(
                                        (PI / 180.) * (tank.angel as f32 - 90.),
                                    );
                                }
                            }
                            if tank.angel > 90. {
                                format!("{}°", -(180. - tank.angel))
                            } else {
                                format!("{}°", tank.angel)
                            }
                        },
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 15.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    }, //_ => {"Error".to_string()},
                }];
            }
        }
    }
}
fn spawn_help(
    mut commands: Commands,
    font_assets: Res<FontAssets>,
    materials: Res<Materials>,
    _win_size: Res<WinSize>,
) {
    let font_color = Color::rgb(0.9, 0.9, 0.9);
    let font_size = 20.;

    let helps = [
        ("H", "<-"),
        ("L", "->"),
        ("up", "+p"),
        ("down", "-p"),
        ("->", "+w"),
        ("->", "-w"),
        ("Enter", "next"),
    ];

    let mut help: [Option<Entity>; 7] = [None; 7];
    for i in 0..helps.len() {
        help[i] = Some(
            commands
                .spawn_bundle(NodeBundle {
                    style: Style {
                        align_items: AlignItems::Center,
                        size: Size::new(Val::Px(200.), Val::Undefined),
                        ..Default::default()
                    },
                    color: materials.trans,
                    ..Default::default()
                })
                .with_children(|parent| {
                    parent
                        .spawn_bundle(TextBundle {
                            style: Style {
                                ..Default::default()
                            },
                            visibility: Visibility { is_visible: false },
                            text: Text {
                                sections: vec![TextSection {
                                    value: format!("  {}  =  {}", helps[i].0, helps[i].1),
                                    style: TextStyle {
                                        font: font_assets.fira_sans.clone(),
                                        font_size,
                                        color: font_color,
                                    },
                                }],

                                alignment: Default::default(),
                            },
                            ..Default::default()
                        })
                        .insert(HelpDisplay);
                })
                .id(),
        )
    }
    let mut help_et: [Entity; 7] = [help[0].unwrap(); 7];
    for (y, x) in help.into_iter().enumerate() {
        help_et[y] = x.unwrap();
    }
    commands
        .spawn_bundle(NodeBundle {
            color: materials.uiblack,
            style: Style {
                position_type: PositionType::Absolute,
                position: UiRect {
                    right: Val::Px(10.),
                    top: Val::Px(52.),

                    ..Default::default()
                },
                align_items: AlignItems::Center,
                flex_direction: FlexDirection::ColumnReverse,
                size: Size::new(Val::Px(200.), Val::Px(300.)),
                ..Default::default()
            },
            visibility: Visibility { is_visible: false },
            ..Default::default()
        })
        .insert(HelpDisplay)
        .push_children(&help.map(|et| et.unwrap()));
}
fn spawn_bar(mut commands: Commands, font_assets: Res<FontAssets>, materials: Res<Materials>) {
    let bar: f32 = 40.;
    let hbar: f32 = bar / 2.;
    let font_size = 15.;
    let font_color = Color::rgb(0.9, 0.9, 0.9);
    let widght = 65.;
    let spacer1 = commands
        .spawn_bundle(NodeBundle {
            style: Style {
                size: Size::new(Val::Px(20.0), Val::Px(hbar)),
                align_items: AlignItems::Center,
                ..Default::default()
            },
            color: materials.trans,
            ..Default::default()
        })
        .id();

    let spacer2 = commands
        .spawn_bundle(NodeBundle {
            style: Style {
                size: Size::new(Val::Px(20.0), Val::Px(hbar)),
                align_items: AlignItems::Center,
                ..Default::default()
            },
            color: materials.trans,
            ..Default::default()
        })
        .id();

    let wind = commands
        .spawn_bundle(NodeBundle {
            style: Style {
                size: Size::new(Val::Px(60.0), Val::Px(bar)),
                align_items: AlignItems::Center,
                ..Default::default()
            },
            color: materials.trans,
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(TextBundle {
                    text: Text {
                        sections: vec![TextSection {
                            value: "Wind:50".to_string(),
                            style: TextStyle {
                                font: font_assets.fira_sans.clone(),
                                font_size: 15.0,
                                color: Color::rgb(0.9, 0.9, 0.9),
                            },
                        }],
                        alignment: Default::default(),
                    },
                    ..Default::default()
                })
                .insert(Display)
                .insert(WindDisplay)
                .insert(DisplayType::Wind);
        })
        .id();

    let player = commands
        .spawn_bundle(NodeBundle {
            style: Style {
                align_items: AlignItems::Center,
                size: Size::new(Val::Px(widght), Val::Undefined),
                ..Default::default()
            },
            color: materials.trans,
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(TextBundle {
                    style: Style {
                        ..Default::default()
                    },
                    text: Text {
                        sections: vec![TextSection {
                            value: "...".to_string(),
                            style: TextStyle {
                                font: font_assets.fira_sans.clone(),
                                font_size,
                                color: font_color,
                            },
                        }],

                        alignment: Default::default(),
                    },
                    ..Default::default()
                })
                .insert(Display)
                .insert(PlayerDisplay)
                .insert(DisplayType::Player);
        })
        .id();

    let life = commands
        .spawn_bundle(NodeBundle {
            style: Style {
                align_items: AlignItems::Center,
                size: Size::new(Val::Px(widght), Val::Undefined),
                ..Default::default()
            },
            color: materials.trans,
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(TextBundle {
                    style: Style {
                        ..Default::default()
                    },
                    text: Text {
                        sections: vec![TextSection {
                            value: "100%".to_string(),
                            style: TextStyle {
                                font: font_assets.fira_sans.clone(),
                                font_size,
                                color: font_color,
                            },
                        }],

                        alignment: Default::default(),
                    },
                    ..Default::default()
                })
                .insert(Display)
                .insert(LifeDisplay)
                .insert(DisplayType::Life);
        })
        .id();

    let shield = commands
        .spawn_bundle(NodeBundle {
            style: Style {
                align_items: AlignItems::Center,
                //justify_content: JustifyContent::FlexStart,
                size: Size::new(Val::Px(widght), Val::Undefined),
                ..Default::default()
            },
            color: materials.trans,
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(TextBundle {
                    style: Style {
                        ..Default::default()
                    },
                    text: Text {
                        sections: vec![TextSection {
                            value: "None".to_string(),
                            style: TextStyle {
                                font: font_assets.fira_sans.clone(),
                                font_size,
                                color: font_color,
                            },
                        }],

                        alignment: Default::default(),
                    },
                    ..Default::default()
                })
                .insert(Display)
                .insert(ShieldDisplay)
                .insert(DisplayType::Shield);
        })
        .id();

    let angel = commands
        .spawn_bundle(NodeBundle {
            style: Style {
                align_items: AlignItems::Center,
                size: Size::new(Val::Px(widght), Val::Undefined),
                ..Default::default()
            },
            color: materials.trans,
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(TextBundle {
                    style: Style {
                        ..Default::default()
                    },
                    text: Text {
                        sections: vec![TextSection {
                            value: "30a°°°bgefucktescheisse".to_string(),
                            style: TextStyle {
                                font: font_assets.fira_sans.clone(),
                                font_size,
                                color: font_color,
                            },
                        }],

                        alignment: Default::default(),
                    },
                    ..Default::default()
                })
                .insert(Display)
                .insert(AngelDisplay)
                .insert(DisplayType::Angel);
        })
        .id();

    let speed = commands
        .spawn_bundle(NodeBundle {
            style: Style {
                align_items: AlignItems::Center,
                size: Size::new(Val::Px(widght), Val::Undefined),
                ..Default::default()
            },
            color: materials.trans,
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(TextBundle {
                    style: Style {
                        //justify_content: JustifyContent::FlexStart,
                        align_items: AlignItems::Center,
                        ..Default::default()
                    },
                    text: Text {
                        sections: vec![TextSection {
                            value: "20m/s".to_string(),
                            style: TextStyle {
                                font: font_assets.fira_sans.clone(),
                                font_size,
                                color: font_color,
                            },
                        }],

                        alignment: TextAlignment {
                            // I could remove this and nothing would change
                            vertical: VerticalAlign::Center,
                            horizontal: HorizontalAlign::Left,
                        },
                    },
                    ..Default::default()
                })
                .insert(Display)
                .insert(SpeedDisplay)
                .insert(DisplayType::Speed);
        })
        .id();

    let weapon = commands
        .spawn_bundle(NodeBundle {
            style: Style {
                align_items: AlignItems::Center,
                size: Size::new(Val::Px(widght), Val::Undefined),
                ..Default::default()
            },
            color: materials.trans,
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(TextBundle {
                    style: Style {
                        ..Default::default()
                    },
                    text: Text {
                        sections: vec![TextSection {
                            value: "5 Missile".to_string(),
                            style: TextStyle {
                                font: font_assets.fira_sans.clone(),
                                font_size,
                                color: font_color,
                            },
                        }],

                        alignment: Default::default(),
                    },
                    ..Default::default()
                })
                .insert(Display)
                .insert(WeaponDisplay)
                .insert(DisplayType::Weapon);
        })
        .id();

    //player, life, shield
    let row1 = commands
        .spawn_bundle(NodeBundle {
            color: materials.trans,
            style: Style {
                align_items: AlignItems::Center,
                size: Size::new(Val::Percent(100.), Val::Px(hbar)),
                ..Default::default()
            },
            ..Default::default()
        })
        .push_children(&[spacer1, player, life, shield])
        .id();

    //angel, speed, weapon
    let row2 = commands
        .spawn_bundle(NodeBundle {
            color: materials.trans,
            style: Style {
                align_items: AlignItems::Center,
                //justify_content : JustifyContent::FlexEnd,
                size: Size::new(Val::Percent(100.), Val::Px(hbar)),
                ..Default::default()
            },

            ..Default::default()
        })
        .push_children(&[spacer2, angel, speed, weapon])
        .id();

    let left = commands
        .spawn_bundle(NodeBundle {
            color: materials.trans,
            style: Style {
                flex_direction: FlexDirection::Column,
                size: Size::new(Val::Percent(100.), Val::Percent(100.)), //,Val::Undefined),
                ..Default::default()
            },
            ..Default::default()
        })
        .push_children(&[row2, row1])
        .id();

    let pause = commands
        .spawn_bundle(ButtonBundle {
            style: Style {
                size: Size::new(Val::Px(hbar * 1.5), Val::Px(hbar * 1.5)),
                margin: UiRect::all(Val::Px(hbar * 0.25)),
                //margin: UiRect::all(Val::Auto),
                //flex_direction: FlexDirection::ColumnReverse,
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,
                ..Default::default()
            },
            color: UiColor(materials.pause_button.normal),
            ..Default::default()
        })
        .insert(PauseButton)
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text {
                    sections: vec![TextSection {
                        value: "H".to_string(),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 20.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    }],
                    alignment: Default::default(),
                },
                ..Default::default()
            });
        })
        .insert(PauseButton)
        .id();

    let right = commands
        .spawn_bundle(NodeBundle {
            color: materials.trans,
            style: Style {
                //size : Size::new(Val::Px(20.),Val::Px(50.)),
                ..Default::default()
            },
            ..Default::default()
        })
        .push_children(&[wind, pause])
        .id();

    commands
        .spawn_bundle(NodeBundle {
            color: materials.trans,
            style: Style {
                align_items: AlignItems::FlexEnd,
                size: Size::new(Val::Percent(100.), Val::Percent(100.)), //,Val::Undefined),
                ..Default::default()
            },
            ..Default::default()
        })
        .with_children(|parent| {
            parent
                .spawn_bundle(NodeBundle {
                    color: materials.uiblack,

                    style: Style {
                        justify_content: JustifyContent::SpaceBetween,
                        size: Size::new(Val::Percent(100.), Val::Px(bar)), //,Val::Undefined),
                        ..Default::default()
                    },
                    ..Default::default()
                })
                .push_children(&[left, right]);
        });
}
