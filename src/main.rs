mod game;
mod glue;
mod prep;
//mod audio;
//mod loading;
//mod menu_ui;
//mod player;
//mod pause;
//mod select_ui;
//mod ui;

use crate::game::menu::OptionPlugin;
//use crate::glue::actions::ActionsPlugin;
use crate::glue::audio::InternalAudioPlugin;
use crate::glue::loading::LoadingPlugin;
use crate::prep::menu_ui::MenuPlugin; // Menu
use crate::prep::select_ui::SelectUiPlugin; // Weapon menu

//use crate::game::terrian::TerrianPlugin; // Weapon menu

//use crate::pause::PausePlugin;
use crate::game::player::PlayerPlugin;
use crate::game::ui::UiPlugin;

use bevy::app::App;
//#[cfg(debug_assertions)]
//use bevy::diagnostic::{FrameTimeDiagnosticsPlugin, LogDiagnosticsPlugin};
use bevy::prelude::*;

//pub struct ButtonMaterials {
//    pub normal: Handle<ColorMaterial>,
//    pub hovered: Handle<ColorMaterial>,
//}
#[allow(dead_code)]
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
enum GameState {
    Loading,
    Playing,
    Playing2,
    Menu,
    Pause,
    WeaponSelect,
}

pub struct GamePlugin;

impl Plugin for GamePlugin {
    fn build(&self, app: &mut App) {
        app.add_state(GameState::Loading)
            .add_plugin(LoadingPlugin)
            .add_plugin(MenuPlugin)
            .add_plugin(UiPlugin)
            .add_plugin(SelectUiPlugin)
            //.add_plugin(PausePlugin)
            //            .add_plugin(ActionsPlugin)
//            .add_plugin(InternalAudioPlugin)
            .add_plugin(OptionPlugin)
            .add_plugin(PlayerPlugin)
//            .add_plugin(TerrianPlugin)
            ;

        //#[cfg(debug_assertions)]
        //{
        //    app.add_plugin(FrameTimeDiagnosticsPlugin::default())
        //        .add_plugin(LogDiagnosticsPlugin::default());
        //}
    }
}

fn main() {
    let mut app = App::new();
    app.insert_resource(Msaa { samples: 1 })
        .insert_resource(ClearColor(Color::rgb(0.7, 0.4, 0.4)))
        .insert_resource(WindowDescriptor {
            width: 1000.,
            height: 750.,
            resizable: false,
            title: "tanks-float-window".to_string(), // ToD    o
            ..Default::default()
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(GamePlugin);

    app.run();
}
