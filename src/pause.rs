use crate::GameState;
use bevy::prelude::*;
use crate::player::Materials;
use crate::player::PauseButton;
use crate::loading::FontAssets;

pub struct PausePlugin;
impl Plugin for PausePlugin {
    fn build (&self,app:&mut AppBuilder){
app
//        app.add_system_set(
//    SystemSet::on_enter(GameState::Pause)
//     .with_system(spawn_pause_ui.system())
//            )
            .add_system_set(
    SystemSet::on_update(GameState::Pause).with_system(stop_pause.system())
                )
                   .add_system_set(
    SystemSet::on_update(GameState::Playing)
        .with_system(pause.system())
                );
//            .add_system_set(
//    SystemSet::on_enter(GameState::Playing)
//
//        .with_system(spawn_pause_button.system())
//
//               );
    }

}
fn stop_pause(
      kb: Res<Input<KeyCode>>,
      mut state: ResMut<State<GameState>>,

    ){
    if kb.just_released(KeyCode::Space) && state.current() == &GameState::Pause {
        state.set(GameState::Playing);
        println!("woaw");
    }

}

fn pause(
    kb: Res<Input<KeyCode>>,
      mut state: ResMut<State<GameState>>,
    ){
    if kb.just_pressed(KeyCode::Space) {
        println!("pause");
        state.set(GameState::Pause);
    }

}

fn spawn_pause_button(
    mut commands: Commands,
    materials: Res<Materials>,
    ){

    commands.spawn_bundle(NodeBundle{
        material : materials.trans.clone(),

          style: Style {

                        justify_content: JustifyContent::SpaceBetween,
                        align_items: AlignItems::FlexEnd,
                        size:Size::new(Val::Percent(100.),Val::Percent(100.)),//,Val::Undefined),
                        //size: Size::new(Val::Px(200.0), Val::Percent(100.0)),

                        ..Default::default()
                    },
                    ..Default::default()
    }).with_children(|parent|
                                          {
                                          parent
        .spawn_bundle(ButtonBundle {
            style: Style {
                margin : Rect::all(Val::Px(10.)),
                size: Size::new(Val::Px(120.0), Val::Px(50.0)),
                //margin: Rect::all(Val::Auto),
                flex_direction: FlexDirection::ColumnReverse,
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,
                ..Default::default()
            },
            material: materials.pause_button.normal.clone(),
            ..Default::default()
        })
        .insert(PauseButton);
           });
}

fn spawn_pause_ui(
    mut commands: Commands,
    font_assets: Res<FontAssets>,
    materials: Res<Materials>,
    ){
    commands.spawn_bundle(NodeBundle{
        material : materials.trans.clone(),

          style: Style {

                        justify_content: JustifyContent::SpaceBetween,
                        align_items: AlignItems::FlexEnd,
                        size:Size::new(Val::Percent(100.),Val::Percent(100.)),//,Val::Undefined),
                        //size: Size::new(Val::Px(200.0), Val::Percent(100.0)),

                        ..Default::default()
                    },
                    ..Default::default()
    }).with_children(|parent|
                                          {
                                          parent
        .spawn_bundle(ButtonBundle {
            style: Style {
                margin : Rect::all(Val::Px(10.)),
                size: Size::new(Val::Px(120.0), Val::Px(50.0)),
                //margin: Rect::all(Val::Auto),
                flex_direction: FlexDirection::ColumnReverse,
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,
                ..Default::default()
            },
            material: materials.pause_button.normal.clone(),
            ..Default::default()
        })
        .insert(PauseButton)
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text {
                    sections: vec![TextSection {
                        value: "Pause".to_string(),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 40.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    }],
                    alignment: Default::default(),
                },
                ..Default::default()
            });
        }
        );
                                     parent
        .spawn_bundle(ButtonBundle {
            style: Style {
                size: Size::new(Val::Px(120.0), Val::Px(50.0)),
                margin : Rect::all(Val::Px(10.)),
                //margin: Rect::all(Val::Auto),
                flex_direction: FlexDirection::ColumnReverse,
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,
                ..Default::default()
            },
            material: materials.pause_button.normal.clone(),
            ..Default::default()
        })
        .insert(PauseButton)
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text {
                    sections: vec![TextSection {
                        value: "Peace".to_string(),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 40.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    }],
                    alignment: Default::default(),
                },
                ..Default::default()
            });
        }
        );


});
}
