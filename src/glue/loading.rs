use crate::GameState;
use bevy::prelude::*;
//use bevy::render::render_resource::Texture;
//use bevy::asset::AssetLoader;
use bevy_asset_loader::prelude::*;
//use bevy_asset_loader::{AssetCollection, AssetLoader};
use bevy_kira_audio::AudioSource;
pub struct LoadingPlugin;

/// This plugin loads all assets using [AssetLoader] from a third party bevy plugin
/// Alternatively you can write the logic to load assets yourself
/// If interested, take a look at https://bevy-cheatbook.github.io/features/assets.html
impl Plugin for LoadingPlugin {
    fn build(&self, app: &mut App) {
        LoadingState::new(GameState::Loading)
            .continue_to_state(GameState::Menu)
            .with_collection::<FontAssets>()
            .with_collection::<TextureAssets>()
//            .with_collection::<AudioAssets>()
            .build(app);
    }
}

// the following asset collections will be loaded during the State `GameState::Loading`
// when done loading, they will be inserted as resources (see https://github.com/NiklasEi/bevy_asset_loader)

#[derive(AssetCollection)]
pub struct FontAssets {
    #[asset(path = "/usr/share/tank-game/assets/fonts/FiraSans-Bold.ttf")]
    //#[asset(path = "fonts/FiraSans-Bold.ttf")]
    pub fira_sans: Handle<Font>,
}
#[derive(AssetCollection)]
pub struct TextureAssets {
    #[asset(path = "/usr/share/tank-game/assets/textures/turret.png")]
    //#[asset(path = "textures/turret.png")]
    pub turret: Handle<Image>,
    #[asset(path = "/usr/share/tank-game/assets/textures/tank.png")]
    //#[asset(path = "textures/tank.png")]
    pub tank: Handle<Image>,
}
#[derive(AssetCollection)]
pub struct AudioAssets {
    #[asset(path = "/usr/share/tank-game/assets/audio/flying.ogg")]
    //#[asset(path = "audio/flying.ogg")]
    pub flying: Handle<AudioSource>,
}
