use crate::game::player::GameSettings;
use crate::game::player::Materials;
use crate::glue::loading::FontAssets;
use crate::GameState;
use bevy_ui::UiRect;
use bevy::prelude::*;
pub struct SelectUiPlugin;

impl Plugin for SelectUiPlugin {
    fn build(&self, app: &mut App) {
        app.init_resource::<ButtonMaterials>()
            .add_system_set(SystemSet::on_enter(GameState::WeaponSelect).with_system(setup_menu))
            .add_system_set(
                SystemSet::on_update(GameState::WeaponSelect)
                    .with_system(click_play_button)
                    .with_system(enter_play),
            );
    }
}

struct ButtonMaterials {
    normal: UiColor,
    _hovered: UiColor,
}

impl FromWorld for ButtonMaterials {
    fn from_world(_world: &mut World) -> Self {
        ButtonMaterials {
            normal: UiColor(Color::rgb(0.15, 0.15, 0.15)),
            _hovered: UiColor(Color::rgb(0.25, 0.25, 0.25)),
        }
    }
}
#[derive(Component)]
struct PlayButton2;
#[derive(Component)]
struct SelectUi;

fn enter_play(
    mut commands: Commands,
    mut state: ResMut<State<GameState>>,
    main_query: Query<Entity, With<SelectUi>>,
    mut kb: ResMut<Input<KeyCode>>,
) {
    if kb.just_released(KeyCode::Return) {
        let main = main_query.single();
        {
            commands.entity(main).despawn_recursive();
        }
        state.set(GameState::Playing).unwrap();
        //kb.reset(KeyCode::Return);
        kb.reset(KeyCode::Return);
    }
}

fn setup_menu(
    materials: Res<Materials>,
    mut commands: Commands,
    font_assets: Res<FontAssets>,
    button_materials: Res<ButtonMaterials>,
) {
    let ui = commands
        .spawn_bundle(ButtonBundle {
            style: Style {
                size: Size::new(Val::Px(120.0), Val::Px(50.0)),
                margin: UiRect::all(Val::Auto),
                justify_content: JustifyContent::Center,
                align_items: AlignItems::Center,

                ..Default::default()
            },
            color: button_materials.normal,
            ..Default::default()
        })
        .insert(PlayButton2)
        .with_children(|parent| {
            parent.spawn_bundle(TextBundle {
                text: Text {
                    sections: vec![TextSection {
                        value: "Play".to_string(),
                        style: TextStyle {
                            font: font_assets.fira_sans.clone(),
                            font_size: 40.0,
                            color: Color::rgb(0.9, 0.9, 0.9),
                        },
                    }],
                    alignment: Default::default(),
                },
                ..Default::default()
            });
        })
        .id();

    commands
        .spawn_bundle(NodeBundle {
            color: materials.trans,
            style: Style {
                size: Size::new(Val::Percent(100.), Val::Percent(100.)),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(SelectUi)
        .push_children(&[ui]);
}

type ButtonInteraction<'a> = (Entity, &'a Interaction, &'a Children);

fn click_play_button(
    mut commands: Commands,
    _button_materials: Res<ButtonMaterials>,
    mut state: ResMut<State<GameState>>,
    settings: Res<GameSettings>,
    mut interaction_query: Query<ButtonInteraction, (Changed<Interaction>, With<Button>)>,
    main_query: Query<Entity, With<SelectUi>>,
) {
    for (_button, interaction, _children) in interaction_query.iter_mut() {
        //let text = text_query.get(children[0]).unwrap();
        match *interaction {
            Interaction::Clicked => {
                let main = main_query.single();
                {
                    commands.entity(main).despawn_recursive();
                }
                if settings.mode == 0 {
                    state.set(GameState::Playing2).unwrap();
                } else {
                    state.set(GameState::Playing).unwrap();
                }
            }
            Interaction::Hovered => {
                //  *material = button_materials.hovered.clone();
            }
            Interaction::None => {
                // *material = button_materials.normal.clone();
            }
        }
    }
}
